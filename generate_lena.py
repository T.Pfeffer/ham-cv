import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

def denoise_ham(img, weight=0.1, eps=1e-3, num_iter_max=2):
  
  max_deformations = 2
  um = [ np.zeros_like(img) for i in range(0,max_deformations+1)]
  
  umx = [ np.roll(img, -1, axis=1) - img for i in range(0,max_deformations+1)]
  umy = [ np.roll(img, -1, axis=0) - img for i in range(0,max_deformations+1)]

  for i in range(1,max_deformations+1):
    if i==1:
      um[1] = np.zeros_like(img)
      umx[1] = np.roll(um[1], -1, axis=1) - um[1]
      umy[1] = np.roll(um[1], -1, axis=0) - um[1]
    else:
      um[2] = np.zeros_like(img)


def denoise(img, weight=0.1, eps=1e-3, num_iter_max=200):
  u = np.zeros_like(img)
  px = np.zeros_like(img)
  py = np.zeros_like(img)

  nm = np.prod(img.shape[:2])
  tau = 0.125

  i = 0
  while i < num_iter_max:
    u_old = u
    # x and y components of u's gradient
    ux = np.roll(u, -1, axis=1) - u
    uy = np.roll(u, -1, axis=0) - u
    # update the dual variable
    px_new = px + (tau / weight) * ux
    py_new = py + (tau / weight) * uy
      
    norm_new = np.maximum(1, np.sqrt(px_new **2 + py_new ** 2))
    px = px_new / norm_new
    py = py_new / norm_new

    # calculate divergence
    rx = np.roll(px, 1, axis=1)
    ry = np.roll(py, 1, axis=0)
    div_p = (px - rx) + (py - ry)

    # update image
    u = img + weight * div_p

    # calculate error
    error = np.linalg.norm(u - u_old) / np.sqrt(nm)

    if i == 0:
      err_init = error
      err_prev = error
    else:
      # break if error small enough
      if np.abs(err_prev - error) < eps * err_init:
        break
      else:
        e_prev = error

    # don't forget to update iterator
    i += 1
  print(i)
  return u

fname = 'lena.png'
image = Image.open(fname).convert("L")
arr = np.asarray(image)
print(arr.shape)
noise = 10 * np.random.normal(0.,1.,arr.shape)
arr_noise = arr + np.clip(noise, 0, 255)

plt.figure( figsize = (12,4) )
plt.subplot(1,3,1)
plt.imshow(arr, cmap='gray', vmin=0, vmax=255)
plt.axis('off')

plt.subplot(1,3,2)
plt.imshow(arr_noise, cmap='gray', vmin=0, vmax=255)
plt.axis('off')

arr_denoised = denoise(arr_noise, 20)

plt.subplot(1,3,3)
plt.imshow(arr_denoised, cmap='gray', vmin=0, vmax=255)
plt.axis('off')

plt.show()

denoise_ham(arr_noise, 20)
